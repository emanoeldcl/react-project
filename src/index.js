import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import numberPhoneList from './texts/numberPhoneList'
import ListPhones from './components/ListPhones';

ReactDOM.render(
  <React.StrictMode>
    <App />
    <ListPhones numbers={numberPhoneList}/>
  </React.StrictMode>,
  document.getElementById('root')
);