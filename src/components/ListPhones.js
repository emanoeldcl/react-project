import React from "react";

function ListPhone(props) {
  return (
    <ul>
      { props.numbers.map(elem => (
        <li key={elem.number}>
          {elem.number}
        </li>
      ))}
    </ul>
  )
}

export default ListPhone;